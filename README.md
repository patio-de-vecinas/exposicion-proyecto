# ClothingDSS-DjangoAngular

Exposición del proyecto realizado para la asignatura Tecnologías de Bases de Datos por el grupo 6.
El código asociado al proyecto está publicado en [este repositorio](https://gitlab.com/patio-de-vecinas/clothingdss-djangoangular).

## Autoría y licencia
| Nombre completo | Perfil de GitLab | Vía de contacto |
| - | - | - |
| Ismael González Sastre | [Ismaelgzse](https://gitlab.com/Ismaelgzse) | - |
| Diego Guerrero Carrasco | [diego-guerrero](https://gitlab.com/diego-guerrero) | [E-mail](mailto:d.guerrero.2018@alumnos.urjc.es), [LinkedIn](https://www.linkedin.com/in/diego-guerrero-carrasco) |
| Andrea Nuzzi Herrero | [AndyNuzzi](https://gitlab.com/AndyNuzzi) | - |
| José Luis Toledano Díaz | [jolutoher18](https://gitlab.com/jolutoher18) | - |
| Flavia Vásquez Gutiérrez | [flavia29](https://gitlab.com/flavia29) | [E-mail](mailto:f.vasquez.2018@alumnos.urjc.es), [LinkedIn](https://www.linkedin.com/in/flavia-vasquez-gutierrez) |

Puede contactar a los integrantes del grupo de desarrollo creando un nuevo *issue* en este repositorio o a través de las vías de contacto introducidas en la tabla anterior.

Este es un proyecto de código abierto (*Open Source*) distribuido bajo licencia Apache 2.0. Se ruega [leer la licencia](https://gitlab.com/patio-de-vecinas/clothingdss-djangoangular/-/raw/main/LICENSE) antes de emplear el código divulgado, siendo necesario reconocer la autoría original al completo antes de redistribuir la versión original o versiones modificadas del producto.
